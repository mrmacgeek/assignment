<?php

namespace Model;


/**
 * @author Wouter Hovius
 */
class Contact extends BO
{
    const DBTABLE = 'contacts';

    private $id;
    private $firstName;
    private $lastName;
    private $phoneNumber;
    private $emailAddress;
    private $company;

    public function __construct($id = null, $firstName, $lastName, $phoneNumber, $emailAddress, $companyId)
    {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->phoneNumber = $phoneNumber;
        $this->emailAddress = $emailAddress;
        $this->company = Company::getById($companyId);
    }

    /**
     * @param $id
     * @return Contact
     */
    public static function getById($id)
    {
        $data = parent::getObjectDataFromDatabase($id, self::DBTABLE);
        $company = new Contact($data['id'], $data['first_name'],$data['last_name'],$data['phone_number'],$data['email_address'],$data['company_id']);
        return $company;
    }

    public static function getAll()
    {
        $data = parent::getAllObjectDataFromDatabase(self::DBTABLE);
        $contacts = [];
        foreach ($data as $row) {
            $contacts[] = new Contact($row['id'], $row['first_name'],$row['last_name'],$row['phone_number'],$row['email_address'],$row['company_id']);
        }
        return $contacts;
    }


}