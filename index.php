<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require __DIR__ . '/autoload.php';
?>


<html>
    <head>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </head>
    <body>
    <table class="table table-striped">
        <tr>
            <td>#</td>
            <td>CompanyName</td>
            <td>Actions</td>
        </tr>
        <?php
            foreach (\Model\Company::getAll() as $company){
                echo "<tr>
                        <td>" . $company->getId() . "</td>
                        <td>" . $company->getName() . "</td>
                        <td><a href='#' class='btn btn-warning'>Edit</a><a href='#' class='btn btn-danger'>Delete</a></td>
                      </tr>";
            }
        ?>
    </table>
    </body>
</html>