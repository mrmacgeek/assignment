<?php

namespace Model;


/**
 * @author Wouter Hovius
 */
class Address extends BO
{
    const DBTABLE = 'addresses';

    private $id;
    private $street;
    private $postalCode;
    private $addressNumber;
    private $city;
    private $country;
    private $company;

    public function __construct($id = null, $street, $postalCode, $addressNumber, $city, $country, $companyId)
    {
        $this->id = $id;
        $this->street = $street;
        $this->postalCode = $postalCode;
        $this->addressNumber = $addressNumber;
        $this->city = $city;
        $this->country = $country;
        $this->company = Company::getById($companyId);
    }

    /**
     * @param $id
     * @return Contact
     */
    public static function getById($id)
    {
        $data = parent::getObjectDataFromDatabase($id, self::DBTABLE);
        $company = new Address($data['id'], $data['street'], $data['postal_code'], $data['address_number'], $data['city'], $data['country'], $data['company_id']);
        return $company;
    }

    public static function getAll()
    {
        $data = parent::getAllObjectDataFromDatabase(self::DBTABLE);
        $companies = [];
        foreach ($data as $row) {
            $companies[] = new Address($row['id'], $row['street'], $row['postal_code'], $row['address_number'], $row['city'], $row['country'], $row['company_id']);
        }
        return $companies;
    }


}