<?php

namespace Model;


/**
 * @author Wouter Hovius
 */
class Company extends BO
{
    const DBTABLE = 'companies';

    private $id;
    private $name;

    public function __construct($id = null, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    public static function getById($id)
    {
        $data = parent::getObjectDataFromDatabase($id, self::DBTABLE);
        $company = new Company($data['id'], $data['name']);
        return $company;
    }

    public static function getAll()
    {
        $data = parent::getAllObjectDataFromDatabase(self::DBTABLE);
        $companies = [];
        foreach ($data as $row) {
            $companies[] = new Company($row['id'], $row['name']);
        }
        return $companies;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }


}